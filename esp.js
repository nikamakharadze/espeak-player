(function(){
	var playButton = document.getElementById('esp-player-icon-play');

	if(playButton !== null){
		playButton.addEventListener('click', function(){
			this.className = (this.className == 'playing') ? '' : 'playing';
		});
	}
})();

